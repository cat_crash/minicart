<?php 
namespace cat_crash\minicart;


Class discount {
	const TYPE_PERCENT = 0;
	const TYPE_AMOUNT = 1;
	const TYPE_FIXRATE = 2;

	public $type;
	public $amount;


	public function __construct($type,$amount){
		$this->type=$type;
		$this->amount=$amount;
	}


	public function applyDiscount($initalAmount){

		switch($this->type){
			default:
			case discount::TYPE_PERCENT:
				$initalAmount=($initalAmount*$this->amount/100);
			break; 

			case discount::TYPE_AMOUNT:
				$initalAmount=($this->amount);
			break; 

			case discount::TYPE_FIXRATE:
				$initalAmount=($this->amount);
			break; 

		}
		return $initalAmount;
	}
	
}
?>