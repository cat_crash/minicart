<?php 
namespace cat_crash\minicart;


Class item {
	public $id;
	public $name;
	public $price;
	public $qty; 


	public function __construct($id,$name,$price,$qty=1){
		$this->id=$id;
		$this->name=$name;
		$this->price=$price;
		$this->qty=$qty;
	}

	public function itemTotal(){
		return $this->qty*$this->price;
	}
	
}
?>