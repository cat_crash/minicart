<?php
namespace cat_crash\minicart;

use Yii;
use yii\base\Component;
use yii\base\Event;
use yii\di\Instance;
use yii\web\Session;

Class minicart extends Component 
{
	protected $session;
    protected $items = [];
    protected $discount=null;
    protected $promo=null;

    protected $discountAmount=0;
    protected $discountObject=null;

    protected $promoAmount=0;
    protected $promoObject=null;


	public function initX(){
		$this->session=Yii::$app->session;

		if(isset($session['minicart'])){
			$this->items=$this->session['minicart']['items'];
			$this->discount=$this->session['minicart']['discount'];
			$this->promo=$this->session['minicart']['promo'];
		} 
	}


	public function put(item $item){
		$this->items[]=$item;
	}

	public function remove($id){
		if(array_key_exists($id, $this->items)){

		}
	}

	private function getItemsTotal(){
		$total=0;

		foreach ($this->items as $item){
			$total=$total+$item->itemTotal();
		}

		return $total;

	}

	public function total(){
		$total=0;

		$total=$total+$this->getItemsTotal();
		$total=$total+($this->discountAmount);
		$total=$total+($this->promoAmount);

		return round($total,2);
	}

	private function subtotal(){
		$total=0;

		$total=$total+$this->getItemsTotal();

		return $total;
	}

	public function getItems(){
		return $this->items;
	}

	public function getPromos(){
		return $this->promoObject;
	}

	public function getDiscounts(){
		return $this->discountObject;
	}

	public function applyDiscount(discount $discount){

		$this->discountObject=$discount;
		$this->discountAmount=$discount->applyDiscount($this->getItemsTotal());

	}

	public function applyPromo(promo $promo){
		$this->promoObject=$promo;
		$this->promoAmount=$promo->applyDiscount($this->getItemsTotal());
	}

	public function __destruct(){
		$this->session['minicart']=serialize($this);
	}


}
?>