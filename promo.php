<?php 
namespace cat_crash\minicart;


Class promo {
	const TYPE_PERCENT = 0;
	const TYPE_AMOUNT = 1;

	public $type;
	public $amount;
	public $code;


	public function __construct($type,$amount,$code){
		$this->type=$type;
		$this->amount=$amount;
		$this->code=$code;
	}


	public function applyDiscount($initalAmount){

		switch($this->type){
			default:
			case discount::TYPE_PERCENT:
				$initalAmount=($initalAmount*$this->amount/100);
			break; 

			case discount::TYPE_AMOUNT:
				$initalAmount=($this->amount);
			break; 

		}
		return $initalAmount;
	}
	
}
?>